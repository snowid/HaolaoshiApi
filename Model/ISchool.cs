﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Model
{
   public interface ISchool
    {
        public int? SchoolId { get; set; }
    }
}
