﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model
{
    [Serializable]
    [Table("User")]
    public class User : Person
    {
        public String Wx_openid { get; set; }
        public String Wx_unionid { get; set; }
        /// <summary>
        /// 绑定教师账号
        /// </summary>
        public int? TeacherId { get; set; }
        [ForeignKey("TeacherId")]
        public virtual Teacher Teacher { get; set; }
        /// <summary>
        /// 绑定学生账号
        /// </summary>
        public int? StudentId { get; set; }
        [ForeignKey("StudentId")]
        public virtual Student Student { get; set; }
    }
}
