﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model
{
    /**
     * 签到日志
     */
    [Serializable]
    [Table("ClockLog")]
    public class ClockLog : SchoolUserStudentID
    {
        public enum ClockStatus
        {
            [Display(Name = "正常")]
            normal = 1,
            [Display(Name = "迟到")]
            late = 2,
            [Display(Name = "缺席")]
            absence = 3
        }
        public int? ClockId { get; set; }
        [ForeignKey("ClockId")]
        [Display(Name = "签到记录")]
        public virtual Clock Clock { get; set; }
        /// <summary>
        /// 签到状态
        /// </summary>
        public ClockStatus Status { get; set; } = ClockStatus.absence;
      
        /**
       * 签到主体。user、student、teacher任选其一
       */
        //public int? TeacherId { get; set; }
        //[ForeignKey("TeacherId")]
        //public virtual Teacher Teacher { get; set; }
        /**
       * 签到主体。user、student、teacher任选其一
       */
        //public int? StudentId { get; set; }
        //[ForeignKey("StudentId")]
        //public virtual Student Student { get; set; }
    }
}