﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Model.Migrations
{
    public partial class haolaoshi33 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Wx_openid",
                table: "User",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Wx_unionid",
                table: "User",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Wx_openid",
                table: "User");

            migrationBuilder.DropColumn(
                name: "Wx_unionid",
                table: "User");
        }
    }
}
