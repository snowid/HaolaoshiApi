﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Model;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model
{
    //招聘岗位
    [Serializable]
    [Table("Post")]
    public class Post : SchoolUserTeacherID
    {
        [Display(Name = "名称")]
        [Required(ErrorMessage = "名称必填")]
        public string Name { get; set; }     
        [Display(Name = "薪资")]
        public string Salary { get; set; }
        [Display(Name = "招聘数量")]
        public int? Num { get; set; }//招聘数量
        [Display(Name = "简历提交")]
        public DateTime? Resume_time { get; set; }
        //面试时间
        [Display(Name = "面试时间")]
        public DateTime? Interview_time { get; set; }//面试时间
        [Display(Name = "面试方式")]
        public string Interview_way { get; set; }
        [Display(Name = "封面")]
        public string Intro { get; set; }
        [Display(Name = "附件")]
        public string Attachment { get; set; }//附件
        public int? CompanyId { get; set; }
        [ForeignKey("CompanyId")]
        [Display(Name = "所属公司")]
        public virtual Company Company { get; set; }
    }
}