﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Model
{
    /// <summary>
    /// 统一
    /// </summary>
   public class SchoolUserID : UserID, ISchool
    {             
        public int? SchoolId { get; set; }
        [ForeignKey("SchoolId")] 
        public virtual School School { get; set; }
    }
}
