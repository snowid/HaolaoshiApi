﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace Model
{
    //课程
    [Serializable]
    [Table("Course")]
    public class Course : SchoolUserTeacherID
    {      
        [Display(Name = "名称")]
        public string Name { get; set; }
        [Display(Name = "编号")]
        public string Sn { get; set; }
        [Display(Name = "封面")]
        public string Pic { get; set; }
        [Display(Name = "教材")]
        public string Textbook { get; set; }
        [Display(Name = "简介")]
        public string Intro { get; set; }
        /// <summary>
        /// 附件，多个用“;”隔开
        /// </summary>
        [Display(Name = "附件")]
        public string Attach { get; set; }
       
        public int? CategoryId { get; set; }
        [ForeignKey("CategoryId")]
        public virtual Category Category { get; set; } 
        //public int? TeacherId { get; set; }
        //[ForeignKey("TeacherId")]
        //public virtual Teacher Teacher { get; set; }
        //public int? StudentId { get; set; }
        //[ForeignKey("StudentId")]
        //public virtual Student Student { get; set; }
        /// <summary>
        /// 公开的
        /// </summary>
        public bool Open { get; set; } = true;
        /// <summary>
        ///  来源或者上级课程，比如学校行政课程，教师上课的课程来源于学校系统行政课程，不同的老师可以根据系统行政课程创建属于自己的课程
        /// </summary>
        public int? ParentId { get; set; }
        /// <summary>
        /// 来源或者上级课程，比如学校行政课程，教师上课的课程来源于学校系统行政课程，不同的老师可以根据系统行政课程创建属于自己的课程
        /// </summary>
        [ForeignKey("ParentId")]
       // [JsonIgnore]
        public virtual Course Parent { get; set; }
    }
}