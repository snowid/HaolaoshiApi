﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Model
{
    /// <summary>
    ///教室
    /// </summary>
    [Serializable]
    [Table("Classroom")]
    public class Classroom : SchoolUserID
    {
        /// <summary>
        /// 教室编号
        /// </summary>
        [Required(ErrorMessage = "教室编号必填")]
        public string Sn { get; set; }
        [Display(Name = "教室名称")]
        [Required(ErrorMessage = "教室名称必填")]
        public string Name { get; set; }
        public ClassroomType Type { get; set; }//教室类型
        /// <summary>
        /// 容量
        /// </summary>
        public int Capacity { get; set; }
    }
    public enum ClassroomType
    {
        [Display(Name = "多媒体教室")]
        MultiMedia = 1,
        [Display(Name = "计算机机房")]
        ComputerRoom = 2,
        [Display(Name = "会议厅")]
        Meeting = 3
    }
}
