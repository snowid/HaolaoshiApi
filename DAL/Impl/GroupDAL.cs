using System;
using System.Collections.Generic;
using System.Text;
using Model;
namespace DAL.Impl
{

    public class GroupDAL : BaseDAL<Group>, IGroupDAL
    {
        public GroupDAL(MyDbContext db) : base(db)
        {
        }
    }
}
