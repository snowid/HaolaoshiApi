using System;
using System.Collections.Generic;
using System.Text;
using Model;
namespace DAL.Impl
{

    public class CourseClassDAL : BaseDAL<CourseClass>, ICourseClassDAL
    {
        public CourseClassDAL(MyDbContext db) : base(db)
        {
        }
    }
}
