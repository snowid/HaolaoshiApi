using System;
using System.Collections.Generic;
using System.Text;
using Model;
namespace DAL.Impl
{

    public class ClassroomDAL : BaseDAL<Classroom>, IClassroomDAL
    {
        public ClassroomDAL(MyDbContext db) : base(db)
        {
        }
    }
}
