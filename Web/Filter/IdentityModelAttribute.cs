﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.Filter
{
    /// <summary>
    /// 自定义特性 ，用于判断是否需要给model添加身份标志
    /// </summary>
    [AttributeUsage(AttributeTargets.Method,
    AllowMultiple = false)]
    public class IdentityModelAttribute : System.Attribute
    {
    }
}
