﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Weixin
{
    public class WxConfig
    {
        public WxMiniConfig Mini { get; set; }
        public WxMpConfig Mp { get; set; }    
    }
    public class WxMiniConfig
    {
        public string AppID { get; set; }
        public string AppSecret { get; set; }     
    }
    public class WxMpConfig: WxMiniConfig
    {       
        public string Token { get; set; }
    }
}
