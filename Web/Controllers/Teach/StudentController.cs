﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Model;
using Bll;
using Common;
using Web.Extension;
using Web.Util;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Authorization;
using System.IO;
using System.Data;
using Web.Redis;
using Common.Util;
using Web.Controllers;
using Web.Filter;
namespace Web.Teach.Controllers
{
    [Route("api/teach/[controller]/[action]")]
    [ApiController]
    [Authorize("teacher")]
    [IdentityModelActionFilter]
    public class StudentController : MyBaseController
    {
        public IClasssBll classsBll { get; set; }
        IStudentBll bll;
        private readonly IWebHostEnvironment webHostEnvironment;
        public StudentController(IStudentBll bll, IWebHostEnvironment hostingEnvironment)
        {
            this.bll = bll;
            this.webHostEnvironment = hostingEnvironment;
        }
        // GET: api/List/Student
        [HttpGet]
        public Result List([FromQuery] Dictionary<string, string> where)
        {
            where.Remove("TeacherId");
            return Result.Success("succeed").SetData(bll.Query(where));
        }

        // GET: api/Student/Get/5
        [HttpGet("{id}")]
        public Result Get([FromQuery] Dictionary<string, string> where)
        {
            return Result.Success("succeed").SetData(bll.SelectOne(where));
        }
        // POST: api/Student/Add
        [HttpPost]
        public Result Add(Student o)
        {
            return ModelState.IsValid ? (bll.Add(o) ? Result.Success("添加成功") : Result.Error("添加失败")) : Result.Error("添加失败!" + ModelState.GetAllErrMsgStr(";")); ;
        }

        // Post: api/Student/Update
        [HttpPost]
        public Result Update(Student o)
        {
            return ModelState.IsValid ? (bll.Update(o) ? Result.Success("修改成功").SetData(o) : Result.Error("修改失败")) : Result.Error("修改失败!" + ModelState.GetAllErrMsgStr(";")); ;
        }

        // Get: api/Student/Delet/5
        [HttpGet("{id}")]
        public Result Delete([FromQuery] Dictionary<string, string> where)
        {
            return bll.Delete(where) ? Result.Success("删除成功") : Result.Error("删除失败");
        }
        [HttpPost]
        public Result BatchDelete([FromForm] Dictionary<string, string> where)
        {
            return bll.Delete(where) ? Result.Success("删除成功") : Result.Error("删除失败");
        }     
    }
}
