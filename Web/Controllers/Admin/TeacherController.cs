﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Model;
using Bll;
using Common;
using Web.Extension;
using Microsoft.AspNetCore.Authorization;
using Web.Controllers;
using System.IO;
using System.Data;
using Web.Util;
using Common.Util;
using Web.Filter;
namespace Web.Admin.Controllers
{
    [Route("api/admin/[controller]/[action]")]
    [ApiController]
    [Authorize("admin")]
    [IdentityModelActionFilter]
    public class TeacherController : MyBaseController
    {
        ITeacherBll bll;
        public TeacherController(ITeacherBll bll)
        {
            this.bll = bll;
        }
        // GET: api/List/Teacher
        [HttpGet]
        public Result List([FromQuery] Dictionary<string, string> where)
        {
            //return Result.Success("succeed").SetData(bll.SelectAll(o => true, pageNo, pageSize));
            return Result.Success("succeed").SetData(bll.Query(where));
        }

        // GET: api/Teacher/Get/5
        [HttpGet("{id}")]
        public Result Get([FromQuery] Dictionary<string, string> where)
        {
            return Result.Success("succeed").SetData(bll.SelectOne(where));
        }
        // POST: api/Teacher/Add
        [HttpPost]
        public Result Add(Teacher o)
        {
            return ModelState.IsValid ? (bll.Add(o) ? Result.Success("添加成功") : Result.Error("添加失败")) : Result.Error("添加失败!" + ModelState.GetAllErrMsgStr(";")); ;
        }

        // Post: api/Teacher/Update
        [HttpPost]
        public Result Update(Teacher o)
        {
            return ModelState.IsValid ? (bll.Update(o) ? Result.Success("修改成功").SetData(o) : Result.Error("修改失败")) : Result.Error("修改失败!" + ModelState.GetAllErrMsgStr(";")); ;
        }

        // Get: api/Teacher/Delet/5
        [HttpGet("{id}")]
        public Result Delete([FromQuery] Dictionary<string, string> where)
        {
            return bll.Delete(where) ? Result.Success("删除成功") : Result.Error("删除失败");
        }
        [HttpPost]
        public Result BatchDelete([FromForm] Dictionary<string, string> where)
        {
            return bll.Delete(where) ? Result.Success("删除成功") : Result.Error("删除失败");
        }
        private string[] imgExt = { ".xlsx", ".xls" };
        private int imgSizeLimit = 5000 * 1024;
         
        [HttpPost()]
        //public async Task<Result> Import(IFormFile file,[FromBody]int classsId)
        public Result Import([FromForm] IFormCollection formCollection)
        {
            var schoolId = Convert.ToInt32(formCollection["schoolId"]);
            FormFileCollection fileCollection = (FormFileCollection)formCollection.Files;
            foreach (IFormFile file in fileCollection)
            {
                var ext = Path.GetExtension(file.FileName).ToLowerInvariant();
                if (string.IsNullOrEmpty(ext) || !imgExt.Contains(ext))
                {
                    return Result.Error("上传失败,请选择xlsx类型文件");
                }
                if (file.Length > 0)
                {
                    if (file.Length > imgSizeLimit)
                    {
                        var size = imgSizeLimit / 1024;
                        return Result.Error($"上传失败,文件超过大小{size}KB");
                    }
                    string filePath = "/temp/" + file.FileName;
                    DataTable dt = OfficeHelper.ReadExcelToDataTable(file.OpenReadStream());
                    foreach (DataRow row in dt.Rows)
                    {
                        Teacher s = new Teacher();
                        s.Username = row["工号"].ToString();
                        s.Role = "teacher";
                        s.Pswd = row["工号"].ToString();
                        s.Sn = row["工号"].ToString();
                        s.Timetable_Sn= row["排课编号"].ToString();
                        s.Gender = Gender.man.Parse<Gender>(row["性别"].ToString());
                        s.Realname = row["姓名"].ToString();
                        s.Nation = row["民族"].ToString();
                        s.IDCard = row["身份证号"].ToString();
                        s.Address = row["通信地址"].ToString();
                        s.Email = row["电子邮箱"].ToString();
                        s.Zip = row["邮编"].ToString();
                        s.Photo = "/upload/teacher/" + schoolId + "/" + s.Realname + ".jpg";
                        s.Tel = row["联系电话"].ToString();
                        s.Education = row["学历"].ToString();
                        s.Graduate_school = row["毕业学校"].ToString();
                        s.Emergency_number = row["紧急联系电话"].ToString();
                        s.Credentials = row["资格证书"].ToString();
                        s.SchoolId = schoolId; 
                        this.bll.Add(s);
                    }
                    //using (var stream = System.IO.File.Create(webHostEnvironment.WebRootPath + filePath))
                    //{

                    //    //await file.CopyToAsync(stream);
                    //}
                    return Result.Success("导入成功");
                }
                else
                {
                    return Result.Success("导入失败,空文件");
                }
            }
            return Result.Success("导入失败,请上传文件");
        }
    }
}
