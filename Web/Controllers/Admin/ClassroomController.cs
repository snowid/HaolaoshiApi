using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Model;
using Bll;
using Common;
using Web.Extension;
using Microsoft.AspNetCore.Authorization;
using Web.Controllers;
using System.IO;
using System.Data;
using Web.Util;
using Web.Filter;

namespace Web.Admin.Controllers
{
    [Route("api/admin/[controller]/[action]")]
    [ApiController]
    [Authorize("admin")]
    [IdentityModelActionFilter]
    public class ClassroomController : MyBaseController
    {
        IClassroomBll bll;
        public ClassroomController(IClassroomBll bll)
        {
            this.bll = bll;
        }
        // GET: api/List/Classroom
        [HttpGet]
        public Result List([FromQuery] Dictionary<string, string> where)
        {
            //return Result.Success("succeed").SetData(bll.SelectAll(o => true, pageNo, pageSize));
            return Result.Success("succeed").SetData(bll.Query(where));
        }

        // GET: api/Classroom/Get/5
        [HttpGet("{id}")]
        public Result Get([FromQuery] Dictionary<string, string> where)
        {
            return Result.Success("succeed").SetData(bll.SelectOne(where));
        }
        // POST: api/Classroom/Add
        [HttpPost]
        public Result Add(Classroom o)
        {
            return ModelState.IsValid ? (bll.Add(o) ? Result.Success("添加成功") : Result.Error("添加失败")) : Result.Error("添加失败!" + ModelState.GetAllErrMsgStr(";")); ;
        }

        // Post: api/Classroom/Update
        [HttpPost]
        public Result Update(Classroom o)
        {
            return ModelState.IsValid ? (bll.Update(o) ? Result.Success("修改成功").SetData(o) : Result.Error("修改失败")) : Result.Error("修改失败!" + ModelState.GetAllErrMsgStr(";")); ;
        }

        // Get: api/Classroom/Delet/5
        [HttpGet("{id}")]
        public Result Delete([FromQuery] Dictionary<string, string> where)
        {
            return bll.Delete(where) ? Result.Success("删除成功") : Result.Error("删除失败");
        }
        [HttpPost]
        public Result BatchDelete([FromForm] Dictionary<string, string> where)
        {
            return bll.Delete(where) ? Result.Success("删除成功") : Result.Error("删除失败");
        }
        [HttpPost()]
        //public async Task<Result> Import(IFormFile file,[FromBody]int classsId)
        public Result Import([FromForm] IFormCollection formCollection)
        {
            //var classsId = Convert.ToInt32(formCollection["classsId"]);
            FormFileCollection fileCollection = (FormFileCollection)formCollection.Files;
            foreach (IFormFile file in fileCollection)
            {
                var ext = Path.GetExtension(file.FileName).ToLowerInvariant();
                if (string.IsNullOrEmpty(ext) || !ExcelExt.Contains(ext))
                {
                    return Result.Error("上传失败,请选择xlsx类型文件");
                }
                if (file.Length > 0)
                {
                    if (file.Length > ImgSizeLimit)
                    {
                        var size = ImgSizeLimit / 1024;
                        return Result.Error($"上传失败,文件超过大小{size}KB");
                    }
                    string filePath = "/temp/" + file.FileName;
                    DataTable dt = OfficeHelper.ReadExcelToDataTable(file.OpenReadStream(), "教室信息");
                    foreach (DataRow row in dt.Rows)
                    {
                        Classroom s = new Classroom();
                        //教室编号
                        var classroomSn = row["教室编号"].ToString();
                        s.Sn = classroomSn;
                        //教室名称
                        var classroomName = row["教室名称"].ToString();
                        s.Name = classroomName;
                        //容量
                        var capacity = row["容量"].ToString();
                        s.Capacity = Convert.ToInt32(capacity);
                        //类型
                        //容量
                        var type = row["类型"].ToString();
                        s.Type = (ClassroomType)Enum.Parse(typeof(ClassroomType), type);
                        this.bll.Add(s);
                    }
                    //using (var stream = System.IO.File.Create(webHostEnvironment.WebRootPath + filePath))
                    //{

                    //    //await file.CopyToAsync(stream);
                    //}
                    return Result.Success("导入成功");
                }
                else
                {
                    return Result.Success("导入失败,空文件");
                }
            }
            return Result.Success("导入失败,请上传文件");
        }
    }
}
