using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Model;
using Bll;
using Common;
using Web.Extension;
using Microsoft.AspNetCore.Authorization;
using Web.Controllers;
using Web.Filter;
using Web.Util;
using Web.Teach.Controllers;
using Quartz;
using Web.Redis;
using Microsoft.Extensions.Logging;
using Web.Controllers.Job;

namespace Web.Admin.Controllers
{
    [Route("api/admin/[controller]/[action]")]
    [ApiController]
    [Authorize("admin")]
    [IdentityModelActionFilter]
    public class ClockController : MyBaseController
    {
        IClockBll bll;
        public IClockBll ClockBll { get; set; }

        public ITimeTableBll timeTableBll { get; set; }
        public ClockController(IClockBll bll)
        {
            this.bll = bll;
        }
        // GET: api/List/Clock
        [HttpGet]
        public Result List([FromQuery] Dictionary<string, string> where)
        {
            //return Result.Success("succeed").SetData(bll.SelectAll(o => true, pageNo, pageSize));
            return Result.Success("succeed").SetData(bll.Query(where));
        }

        // GET: api/Clock/Get/5
        [HttpGet("{id}")]
        public Result Get([FromQuery] Dictionary<string, string> where)
        {
            return Result.Success("succeed").SetData(bll.SelectOne(where));
        }
        // POST: api/Clock/Add
        [HttpPost]
        public Result Add(Clock o)
        {
            return ModelState.IsValid ? (bll.Add(o) ? Result.Success("添加成功") : Result.Error("添加失败")) : Result.Error("添加失败!" + ModelState.GetAllErrMsgStr(";")); ;
        }

        // Post: api/Clock/Update
        [HttpPost]
        public Result Update(Clock o)
        {
            return ModelState.IsValid ? (bll.Update(o) ? Result.Success("修改成功").SetData(o) : Result.Error("修改失败")) : Result.Error("修改失败!" + ModelState.GetAllErrMsgStr(";")); ;
        }

        // Get: api/Clock/Delet/5
        [HttpGet("{id}")]
        public Result Delete([FromQuery] Dictionary<string, string> where)
        {
            return bll.Delete(where) ? Result.Success("删除成功") : Result.Error("删除失败");
        }
        [HttpPost]
        public Result BatchDelete([FromForm] Dictionary<string, string> where)
        {
            return bll.Delete(where) ? Result.Success("删除成功") : Result.Error("删除失败");
        }
        /// <summary>
        /// 开启定时启动签到
        /// </summary>
        /// <param name="offset">偏移量，提前或延后多少分钟开始签到，默认提前20分钟，正提前，负延后</param>
        /// <param name="during">签到时长，分钟，默认30分钟</param>
        /// <returns></returns>
        [HttpPost()]
        public Result StartTimerClock(int offset=20,int during=30)
        {
            var objs = timeTableBll.SelectAll();
            foreach (var obj in objs)
            {
                //开始时间
                var start_time = obj.From_time - new TimeSpan(0, offset, 0);
                var start_h = start_time.Hours;
                var start_m = start_time.Minutes;
                var cors = $"1 {start_m} {start_h} * * ?";
                //结束时间
                //var end_time = obj.From_time + new TimeSpan(0, 10, 0);
                //添加定时任务 在签到结束后将数据持久化到数据库
                Dictionary<string, object> data = new Dictionary<string, object>();
                data.Add("expire", during * 60);
                data.Add("timetable_sn", obj.Sn);
                NLogHelper.logger.Info($"发布定时签到:开始{start_h}:{start_m},cors:{cors}");
                Console.WriteLine($"发布定时签到:开始{start_h}:{start_m},cors:{cors}");
                QuartzHelper.AddAt(typeof(StartTimerClockJob), new JobKey($"TimerClock{obj.Sn}", "TimerClock"), cors, data);
                //QuartzHelper.AddAt(typeof(StartTimerClockJob), new JobKey("TimerClock" + start_time.ToString(@"hh\_mm\_ss"), "TimerClock"), $"0 {start_m} {start_h} * * ?", data);
            }
            MyRedisHelper rd = MyRedisHelper.Instance();
            rd.StringSet("timer_clock_status", true);//定时签到状态
            return Result.Success("发起定时签到成功");
        }
        /// <summary>
        /// 停止定时签到
        /// </summary>
        /// <returns></returns>
        [HttpPost()]
        public Result StopTimerClock()
        {
            var objs = timeTableBll.SelectAll();
            foreach (var obj in objs)
            {
                //开始时间
                var start_time = obj.From_time - new TimeSpan(0, 20, 0);
                var start_h = start_time.Hours;
                var start_m = start_time.Minutes;
                //结束时间   
                QuartzHelper.Stop(new JobKey($"StartTimerClockJob{obj.Sn}"));
                //QuartzHelper.Stop(new JobKey("TimerClock" + start_time.ToString(@"hh\_mm\_ss"), "TimerClock"));
                NLogHelper.logger.Info($"停止定时签到成功:开始{start_h}:{start_m}");
            }
            MyRedisHelper rd = MyRedisHelper.Instance();
            rd.StringSet("timer_clock_status", false);//定时签到状态
            return Result.Success("停止定时签到成功");
        }
        [HttpPost]
        public Result GetTimerClockStatus()
        {
            MyRedisHelper rd = MyRedisHelper.Instance();
            return Result.Success().SetData(rd.StringGet("timer_clock_status"));
        }
    }
}
