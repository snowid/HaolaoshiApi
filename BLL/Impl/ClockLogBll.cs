using System;
using System.Collections.Generic;
using System.Text;
using Model;
using DAL;
namespace Bll.Impl
{
    public class ClockLogBll : BaseBll<ClockLog>, IClockLogBll
    {
        public ClockLogBll(IClockLogDAL dal):base(dal)
        {
        }
    }
}
