using System;
using System.Collections.Generic;
using System.Text;
using Model;
using DAL;
namespace Bll.Impl
{
    public class ClassroomBll : BaseBll<Classroom>, IClassroomBll
    {
        public ClassroomBll(IClassroomDAL dal):base(dal)
        {
        }
    }
}
